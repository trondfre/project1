#script for plotting data from project1.cpp
from numpy import *
from matplotlib.pyplot import *

for i in range(1,4,1):
    n = 10**i
    data = loadtxt("valuesn=%s.dat" % n)

    figure()
    plot(data[:,0], data[:,1])
    plot(data[:,0], data[:,2])
    title("n = %s" % n)
    legend(("computed v(x)", "closed form solution u(x)"))
    xlabel("x")
    ylabel("u(x)")
    savefig("v_and_u_n=%s.pdf" % n)
    show()
