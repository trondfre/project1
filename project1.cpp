#include <iostream>
#include <sstream>
#include <cstdio>
#include <cmath>
#include <time.h>
#include <armadillo>

using namespace std;
using namespace arma;


double *second_derivative(int n, double h, double *w, double *x)
//computes solution of Poisson eq. -u''(x)=f(x) with Dirichlet boundary conditions, 
//by gaussian elimination of Ax=w, where w=h*h*f
{
  double b[n], b_tilde[n+2], b_tilde_star[n];
  x[0] = 0;
  x[n+1] = 0;
  b[0] = 2;
  for (int i=0; i<=n+1; i++){
    b_tilde[i] = h*h*w[i];
  }

  b_tilde_star[0] = b_tilde[1];

  //Gaussian elimination algorithm
      for (int i=1; i<n; i++){
	double term = 1/b[i-1];
	b[i] = b[0] - term;
	b_tilde_star[i] = b_tilde[i+1] + term*b_tilde_star[i-1];
      }

      for (int i=n; i>0; i--){
	x[i] = (b_tilde_star[i-1] + x[i+1])/b[i-1];
      }
      //end of algoritm
      return x;
}  //end of function second derivative

double max_error(int n, double *a, double *b)
//computes relative error of arrays a and b and picks out the max value,
//where a is the computed solution and b is exact
{
     //relative error
      double eps[n];
      for (int i=0; i<n; i++){
	eps[i] = log10(abs((a[i+1] - b[i+1])/b[i+1]));
      }
      //find max value
      double eps_max = eps[0];
      for (int i=1; i<n; i++){
	if (eps[i] > eps_max){
	    eps_max = eps[i];
	  }
      }
      return eps_max;
}  //end of funtion max error

vec LUsolver(int n, double h, double *f)
//uses LU decomposition of A to solve Ax = w, where w=h*h*f
{
      mat A = zeros(n,n), L, U;
      vec c(n);
      vec d(n-1);

      A.diag() = c.fill(2.0);
      A.diag(1) = d.fill(-1.0);
      A.diag(-1) = d;
      lu(L, U, A);

      vec b_vec(n);
      for (int i=0; i<n; i++){
	b_vec[i] = h*h*f[i+1];
      }

      vec y = solve(L, b_vec);
      vec v_vec = solve(U, y);
      return v_vec;
}  //end of function LUsolver

int main()
//calls tridiagonal solver and LU solver for different n, takes the time of these functions, 
//saves data to file and prints to screen
{
  
  for (int k=1; k<=5; k++){
    int n = pow(10, k);  //number of steps
    double h = 1/(n+1.0);  //step length

    double v_temp[n+2], x[n+2], f[n+2], u[n+2];

    for (int i=0; i<=n+1; i++){
      x[i] = i*h;
      f[i] = 100*exp(-10*x[i]); //right hand side of eq.
      u[i] = 1 - (1-exp(-10))*x[i] - exp(-10*x[i]);  //exact solution
    }

    //run tridiagonal solver and take time
    clock_t start, finish;
    start = clock();
    double *v = second_derivative(n, h, f, v_temp);
    finish = clock();
    double trisolver_time = (double)(finish - start)/CLOCKS_PER_SEC;


    double eps_max = max_error(n, v, u);

    cout << "n = " << n << ":" << endl;
    cout << "eps_max = " << eps_max << ", " << "log10(h) = " << log10(h) << endl;
    cout << "time used by tridiagonal solver: " << trisolver_time << " seconds" << endl;

     //save to file
    ostringstream n_string;
    n_string << n; //convert number to string
    string filename = "valuesn=" + n_string.str() + ".dat";
    const char *filename2 = filename.c_str();

    FILE *output_file;
    output_file = fopen(filename2, "w");
    for (int i=0; i<n+2; i++){
      fprintf(output_file, "%12.5E %12.5E %12.5E \n", x[i], v[i], u[i]);
    }
    fclose(output_file);

    if (k<=3){
      //run LU solver and take time
      clock_t start, finish;
      start = clock();
      vec v_LU = LUsolver(n, h, f);
      finish = clock();
      double LU_time = (double)(finish - start)/CLOCKS_PER_SEC;
      cout << "time used by LU solver: " << LU_time << " seconds" << endl << endl;
    }
    else{
      cout << endl;
    }
  }
  return 0;
  }  //end of function main
